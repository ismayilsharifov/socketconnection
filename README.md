Source code contains different versions of Socket connection: 
- Simple 
- Single threaded
- Multhithreaded 
- Threadpooled 
- Nio receiver

Threadpooled server socket can accept more than 1000 requests per second.
Used synchronized monitor object as mutex, concurenthashmap as inner cache for monitoring thread performance.
For running main class from other device you must change server ip.
For example : `Socket s = new Socket("192.168.0.100", 9000);`

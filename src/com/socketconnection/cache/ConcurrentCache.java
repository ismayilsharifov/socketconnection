package com.socketconnection.cache;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

public class ConcurrentCache<K, V> {

    private final Map<K, Holder<V>> mMap;
    private final long timeToLive;
    private final long cleanUpInterval;
    private final TimerTask cleanUpTask = new TimerTask() {
        @Override
        public void run() {
            long now = System.currentTimeMillis();
            if(!mMap.isEmpty()){
                Iterator<Map.Entry<K, Holder<V>>> mIterator = mMap.entrySet().iterator();
                while(mIterator.hasNext()){
                    long expiry = timeToLive + mIterator.next().getValue().lastAccessed;
                    if(now > expiry){
                        mIterator.remove();
                    }
                }
            }
        }
    };

    public ConcurrentCache(long elementTimeToLiveMillis, long cleanUpIntervalMillis, int cacheSize){
        mMap = new ConcurrentHashMap<>(cacheSize);
        this.timeToLive = elementTimeToLiveMillis;
        this.cleanUpInterval = cleanUpIntervalMillis;
        setupCleanUpProcess();
    }

    public void put(K key, V value){
        mMap.put(key, new Holder<>(value));
    }

    public V putIfAbsent(K key, V value){
        return mMap.putIfAbsent(key, new Holder<>(value)).getValue();
    }

    public V get(K key){
        Holder<V> mHolder = mMap.get(key);
        if(mHolder != null){
            return mHolder.getValue();
        }else{
            return null;
        }
    }

    public boolean containsKey(K key){
        return mMap.containsKey(key);
    }

    public V remove(K key){
        Holder<V> mHolder = mMap.remove(key);
        if(mHolder != null){
            return mHolder.getValue();
        }else{
            return null;
        }
    }

    private void setupCleanUpProcess(){
        new Timer(true).scheduleAtFixedRate(cleanUpTask, cleanUpInterval, cleanUpInterval);
    }

    private class Holder<T>{
        long lastAccessed;
        T value;

        Holder(T value){
            lastAccessed = System.currentTimeMillis();
            this.value = value;
        }
        T getValue(){
            lastAccessed = System.currentTimeMillis();
            return this.value;
        }
    }

    @Override
    public String toString() {
        StringBuilder bd = new StringBuilder();
        for (K key : mMap.keySet()) {
            bd.append("\nRequest processed : " + key + " ; thread : " +
                    mMap.get(key).value + "; last accessed : " +
                    mMap.get(key).lastAccessed + '\n');
        }
        return "ConcurrentCache{" +
                "mMap=" + bd +
                ", timeToLive=" + timeToLive +
                ", cleanUpInterval=" + cleanUpInterval +
                ", count " + mMap.size() +
                "}\n";
    }
}
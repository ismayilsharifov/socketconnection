package com.socketconnection.client;

import java.io.IOException;
import java.net.Socket;

public class Client {
    public static void main(String[] args) {
        try{
            long start = System.currentTimeMillis();
            for (int i = 0; i < 1000; i++) {
                Socket s = new Socket("192.168.0.100",9000);
                System.out.println("sending " + i + " : " + (System.currentTimeMillis()-start));
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
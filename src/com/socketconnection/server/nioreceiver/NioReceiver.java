package com.socketconnection.server.nioreceiver;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.util.Iterator;
import java.util.Set;

public class NioReceiver {
    private int port = -1;
    private static String message = null;
    public void receive() throws IOException {
        // Get the selector
        var selector = Selector.open();
        // Selector is open for making connection
        // Get the server socket channel and register using selector
        ServerSocketChannel SS = ServerSocketChannel.open();
        InetSocketAddress hostAddress = new InetSocketAddress(this.port);
        SS.bind(hostAddress);
        SS.configureBlocking(false);
        int ops = SS.validOps();
        SelectionKey selectKy = SS.register(selector, ops, null);
        for (;;) {
            //Waiting for the select operation...
            int noOfKeys = selector.select();
            // The Number of selected keys are: noOfKeys
            Set selectedKeys = selector.selectedKeys();
            Iterator itr = selectedKeys.iterator();
            while (itr.hasNext()) {
                ByteBuffer buffer = ByteBuffer.allocate(1024 * 60);
                SelectionKey ky = (SelectionKey) itr.next();
                if (ky.isAcceptable()) {
                    // The new client connection is accepted
                    SocketChannel client = SS.accept();
                    client.configureBlocking(false);
                    // The new connection is added to a selector
                    client.register(selector, SelectionKey.OP_READ);
                    // The new connection is accepted from the client: client
                } else if (ky.isReadable()) {
                    // Data is read from the client
                    SocketChannel client = (SocketChannel) ky.channel();
                    String output = null;
                    buffer.clear();
                    int charRead = -1;
                    try {
                        charRead = client.read(buffer);
                    } catch (IOException e) {
                        continue;
                    }
                    if (charRead <= 0) {
                        // client closed
                        client.close();
                    } else {
                        output = new String(buffer.array());
                        message = output;
                        try {
                            new Thread(() -> {
                                processAndStore(message);
                            }).start();
                        } catch (Exception e) {
                            System.err.println("Thread exception:::" + e.getMessage());
                        }
                    } // else if of client.isConnected()
                } // else if of ky.isReadable()
                itr.remove();
            } // end of while loop
        } // end of for loop
    }

    public void processAndStore(String output) {
        String exchangeName = null;
        String dataLine = null;
        String Lines[] = output.split("\r\n");
        for (int i = 0; i < Lines.length; i++) {
            if (Lines[i].contains("Host: ")) {
                exchangeName = Lines[i].substring(6);
            }
            if (Lines[i].isEmpty()) {
                dataLine = Lines[i + 1];
            }
        }
        StringBuffer updatedLastLine = null;
        if (dataLine != null) {
            if (dataLine.contains("POST")) {
                updatedLastLine = new StringBuffer(dataLine.substring(0, dataLine.indexOf("POST")));
            } else {
                updatedLastLine = new StringBuffer(dataLine);
            }
            if (!dataLine.equals("")) {
                try {
                    if (updatedLastLine.lastIndexOf("}") != -1) {
                        updatedLastLine.replace(updatedLastLine.lastIndexOf("}"), updatedLastLine.lastIndexOf("}") + 1, ",\"name\":\"" + exchangeName
                                + "\"}");
                    } else {

                        return;
                    }
                } catch (StringIndexOutOfBoundsException e) {
                    System.out.println(updatedLastLine + "::" + dataLine);
                    System.out.println(e);
                }
//                store(updatedLastLine.toString());
            }
        }
    }

    public NioReceiver(int port) {
        this.port = port;
    }
}
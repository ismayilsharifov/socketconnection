package com.socketconnection.server.threadpooled;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

public class ThreadSocket extends Thread {
    public Socket getSocket() {
        return socket;
    }

    public void setSocket(Socket socket) {
        this.socket = socket;
    }

    private Socket socket;

    ThreadSocket(Socket socket) {
        this.socket = socket;
        this.start();
    }

    @Override
    public void run() {
        try {
            System.out.println("procesing request");
            BufferedReader in = new BufferedReader(new InputStreamReader(
                    socket.getInputStream()));
            PrintWriter out = new PrintWriter(socket.getOutputStream(),
                    true);
            String instring = in.readLine();
            out.println("The server got this: " + instring.toUpperCase());
            socket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

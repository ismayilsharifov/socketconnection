package com.socketconnection.server.threadpooled;

import com.socketconnection.cache.ConcurrentCache;

import java.io.InputStream;
import java.io.OutputStream;
import java.io.IOException;
import java.net.Socket;

public class WorkerRunnable implements Runnable {

    protected Socket clientSocket;
    protected String serverText;

    protected static ConcurrentCache<Long, Thread> concurrentCache = new ConcurrentCache(
            60 * 60 * 1000, 60 * 60 * 60 * 1000, 10000);

    public WorkerRunnable(Socket clientSocket, String serverText) {
        this.clientSocket = clientSocket;
        this.serverText = serverText;
    }

    public void run() {
        try {
            InputStream input = clientSocket.getInputStream();
            OutputStream output = clientSocket.getOutputStream();
            long time = System.currentTimeMillis();
            this.concurrentCache.put(System.currentTimeMillis(), Thread.currentThread());
            output.write(("HTTP/1.1 200 OK\n\nWorkerRunnable: " +
                    this.serverText + " - " +
                    time +
                    "").getBytes());
            output.close();
            input.close();
            System.out.println("Request processed: " + time);
            System.out.println(concurrentCache.toString());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
package com.socketconnection.utility;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;

public class DateTimeUtil {

    public static LocalDateTime getLocalDateTime(Long longValue) {
        return LocalDateTime.ofInstant(
                Instant.ofEpochMilli(longValue), ZoneId.systemDefault());
    }
}
